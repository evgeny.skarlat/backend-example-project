# Backend Exam Project - Burgers API

## Description

Create API (backend code + db) similar to this one [https://punkapi.com/documentation/v2](https://punkapi.com/documentation/v2), but for Burgers.

**IMPORTANT:** In Addition to the described methods in the documentation you need to provide a method for creating new Burger.

**IMPORTANT 2:** You do not need to create all the PARAMs(abv_gt, abv_lt, etc). Just create one for burger_name.

You can use random burger images from around the internet :)


Wether you deploy the api on public endpoint (like heroku) or keep it localhost is up to you and bears no significance on the review of your work.

*This description is purposefully short in order to allow you to be creative*