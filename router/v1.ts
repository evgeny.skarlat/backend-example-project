import * as express from 'express';
import * as BurgerController from '../controllers/BurgerController';

export default (app) => {

    const apiRoutes = express.Router();

    //burgers routers
    apiRoutes.get('/burgers', BurgerController.getAll);
    apiRoutes.get('/burgers/random', BurgerController.random);
    apiRoutes.get('/burger/:id', BurgerController.findById);
    apiRoutes.get('/*', BurgerController.notFound);
    
    apiRoutes.post('/burgers/create', BurgerController.create);
    

    app.use('/api/v1', apiRoutes);
    app.get('/', BurgerController.index);
}