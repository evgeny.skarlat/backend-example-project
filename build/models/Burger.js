"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var BurgerSchema = new Schema({
    calories: {
        type: Number,
        default: '',
        required: true
    },
    image_url: {
        type: String,
        default: '',
        required: true
    },
    description: {
        type: String,
        default: '',
        required: true
    },
    name: {
        type: String,
        default: '',
        required: true
    }
});
BurgerSchema.statics.findByName = function (name, cb) {
    return this.find({ name: new RegExp('^' + name, 'g') }, cb);
};
exports.default = mongoose.model('Burger', BurgerSchema);
//# sourceMappingURL=Burger.js.map