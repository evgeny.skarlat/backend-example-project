"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var BurgerController = require("../controllers/BurgerController");
exports.default = function (app) {
    var apiRoutes = express.Router();
    //burgers routers
    apiRoutes.get('/burgers', BurgerController.getAll);
    apiRoutes.get('/burgers/random', BurgerController.random);
    apiRoutes.get('/burger/:id', BurgerController.findById);
    apiRoutes.get('/*', BurgerController.notFound);
    apiRoutes.post('/burgers/create', BurgerController.create);
    app.use('/api/v1', apiRoutes);
    app.get('/', BurgerController.index);
};
//# sourceMappingURL=v1.js.map