"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Burger_1 = require("../models/Burger");
//index
function index(req, res, next) {
    res.render('index');
}
exports.index = index;
//not found
function notFound(req, res, next) {
    res.status(404).json({ error: 404, message: 'Not Found' });
}
exports.notFound = notFound;
//get all burgers
function getAll(req, res, next) {
    var pagination = {
        page: +req.query.page - 1 || 0,
        per_page: +req.query.per_page || 2
    };
    if (req.query.burger_name) {
        var burger_name = req.query.burger_name;
        burger_name = burger_name[0].toUpperCase() + burger_name.slice(1);
        Burger_1.default.findByName(burger_name)
            .skip(pagination.page * pagination.per_page)
            .limit(pagination.per_page)
            .then(function (burgers) {
            res.status(200).json({ burgers: burgers });
        })
            .catch(function (err) {
            res.status(500).json({ err: err });
        });
    }
    else {
        Burger_1.default.find()
            .skip(pagination.page * pagination.per_page)
            .limit(pagination.per_page)
            .then(function (burgers) {
            res.status(200).json({ burgers: burgers });
        })
            .catch(function (err) {
            res.status(500).json({ err: err });
        });
    }
}
exports.getAll = getAll;
//find By Id
function findById(req, res, next) {
    var id = req.params.id;
    Burger_1.default.findById(id)
        .then(function (burger) {
        res.status(200).json({ burger: burger });
    })
        .catch(function (err) {
        res.status(500).json({ err: err });
    });
}
exports.findById = findById;
//create
function create(req, res, next) {
    var name = req.body.name;
    var description = req.body.description;
    var image_url = req.body.image_url;
    var calories = req.body.calories;
    if (!name || !description || !image_url || !calories) {
        res.status(500).json({ error: 'All fields are required.' });
    }
    var burger = new Burger_1.default({
        name: name,
        description: description,
        image_url: image_url,
        calories: calories
    });
    burger.save()
        .then(function (burger) {
        res.status(200).json({ burger: burger });
    })
        .catch(function (err) {
        res.status(500).json({ err: err });
    });
}
exports.create = create;
//random
function random(req, res, next) {
    Burger_1.default.find()
        .then(function (burgers) {
        var min = 0, max = burgers.length, random = Math.floor(Math.random() * (max - min)) + min, burger = burgers[random];
        res.status(200).json({ burger: burger });
    });
}
exports.random = random;
//# sourceMappingURL=BurgerController.js.map