var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BurgerSchema = new Schema({
    calories: {
        type: Number,
        default: '',
        required: true
    },
    image_url: {
        type: String,
        default: '',
        required: true
    },
    description: {
        type: String,
        default: '',
        required: true
    },
    name: {
        type: String,
        default: '',
        required: true
    }
});

BurgerSchema.statics.findByName = function(name, cb) {
    return this.find({ name: new RegExp('^'+name, 'g') }, cb);
};

export default mongoose.model('Burger', BurgerSchema);