import Burger from '../models/Burger';

//index
export function index(req, res, next) {
    res.render('index');
}

//not found
export function notFound(req, res, next) {
    res.status(404).json({ error: 404, message: 'Not Found'});
}


//get all burgers
export function getAll(req, res, next) {

    let pagination = {
        page: +req.query.page - 1 || 0,
        per_page: +req.query.per_page || 2
    }

    if (req.query.burger_name) {
        let burger_name = req.query.burger_name;
        burger_name = burger_name[0].toUpperCase() + burger_name.slice(1);

        Burger.findByName(burger_name)
            .skip(pagination.page * pagination.per_page)
            .limit(pagination.per_page)
            .then(burgers => {
                res.status(200).json({ burgers });
            })
            .catch(err => {
                res.status(500).json({ err });
            })
    } else {
        Burger.find()
            .skip(pagination.page * pagination.per_page)
            .limit(pagination.per_page)
            .then(burgers => {
                res.status(200).json({ burgers });
            })
            .catch(err => {
                res.status(500).json({ err });
            })
    }
}

//find By Id
export function findById(req, res, next) {
    const id = req.params.id;

    Burger.findById(id)
        .then(burger => {
            res.status(200).json({ burger });
        })
        .catch(err => {
            res.status(500).json({ err });
        })
}

//create
export function create(req, res, next) {

    const name = req.body.name;
    const description = req.body.description;
    const image_url = req.body.image_url;
    const calories = req.body.calories;

    if(!name || !description || !image_url || !calories) {
        res.status(500).json({ error: 'All fields are required.' });
    }

    const burger = new Burger({
        name,
        description,
        image_url,
        calories
    });

    burger.save()
        .then(burger => {
            res.status(200).json({ burger });
        })
        .catch(err => {
            res.status(500).json({ err });
        })
}

//random
export function random(req, res, next) {
    Burger.find()
        .then(burgers => {
            let min = 0,
                max = burgers.length,
                random = Math.floor(Math.random() * (max - min)) + min,
                burger = burgers[random];
    
            res.status(200).json({ burger });
        })
}